package net.gogo.ampasc2dm;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class GCMBroadcastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);

		String messageType = gcm.getMessageType(intent);
		if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
			sendNotification(context, "Notification error");
		} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
			sendNotification(context, "Message deleted");
		} else {
			Log.e("Consooole", intent.getExtras().toString());
			sendNotification(context, "Received" + intent.getExtras().toString());
		}
	}

	private void sendNotification(Context context, String message) {
		mNotification = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		PendingIntent intent = PendingIntent.getActivity(context, 0,
				new Intent(context, MainActivity.class), 0);

		mBuilder = new NotificationCompat.Builder(context)
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle("GCM Notif").setContentText(message)
				.setContentIntent(intent);

		mNotification.notify(1, mBuilder.build());
	}

	private NotificationManager mNotification;
	private NotificationCompat.Builder mBuilder;
}
