package net.gogo.ampasc2dm;

import java.io.IOException;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mEchoButton = (Button) findViewById(R.id.hello);
		mEchoButton.setOnClickListener(this);

		// get device registration ID from preferences
		mPreference = getSharedPreferences("Ganteng", 0);
		mRegistrationID = mPreference.getString(KEY_REGISTRATION_ID, null);

		mGCM = GoogleCloudMessaging.getInstance(this);
		if (mRegistrationID == null) {
			// should register to GCM
			Toast.makeText(MainActivity.this, "Registering device", Toast.LENGTH_SHORT).show();
			registerDevice();
		}
	}

	@Override
	public void onClick(View v) {
		if (v == mEchoButton) {
			new AsyncTask<Void, Void, Void>() {
				@Override
				protected Void doInBackground(Void... params) {
					Bundle data = new Bundle();
					data.putString("hello", "Hello World");
					try {
						// send data to server
						mGCM.send(GCM_SENDER_ID + "@gcm.googleapis.com", "" + mMessageID, data);
						mMessage = "Sending message to server";
					} catch (IOException iEx) {
						Log.e("Conso", iEx.toString());
						Toast.makeText(MainActivity.this, "Error while sending message", Toast.LENGTH_SHORT).show();
					}
					mMessageID++;
					return null;
				}

				protected void onPostExecute(Void result) {
					Toast.makeText(MainActivity.this, mMessage, Toast.LENGTH_SHORT).show();
				}

				private String mMessage;
			}.execute(null, null, null);
			
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	private void registerDevice() {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				try {
					mRegistrationID = mGCM.register(GCM_SENDER_ID);
					// save registration ID for future use
					SharedPreferences.Editor editor = mPreference.edit();
					editor.putString(KEY_REGISTRATION_ID, mRegistrationID);
					editor.commit();
				} catch (IOException iEx) {
					
				}
				return null;
			}

			protected void onPostExecute(Void result) {
				Log.e("Consoool", mRegistrationID);
				Toast.makeText(MainActivity.this, "Device registered " + mRegistrationID, Toast.LENGTH_SHORT).show();
			}

		}.execute(null, null, null);
	}

	private int mMessageID = 0;
	private SharedPreferences mPreference;
	private String mRegistrationID;
	private GoogleCloudMessaging mGCM;
	private Button mEchoButton;

	// this is your project number from Google API Console. It's project number, not project ID
	private final static String GCM_SENDER_ID = "862363578865";

	private final static String KEY_REGISTRATION_ID = "registrationID";
}
